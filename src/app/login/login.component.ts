import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { SnackBar } from 'src/app/login/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  inputElements: any[] = ['input1', 'input2', 'input3', 'input4', 'input5', 'input6', 'input7'];
  @ViewChildren('input') inputControls: QueryList<any>;
  showErrorMsg: boolean;

  inputEleLen;

  constructor(public snackBar: SnackBar) { }

  ngOnInit() {
  }

  keyInput(btnEl: HTMLButtonElement) {
    let btn_val = +btnEl.value;//convert value to number from string.
    let inputsLen = this.inputControls.toArray().length;
    for (let i = 0; i < inputsLen; i++) {
      const input = this.inputControls.toArray()[i].nativeElement;
      if (!input.value) {
        if (i < inputsLen - 1) {
          const nextInput = this.inputControls.toArray()[i + 1].nativeElement;
          nextInput.focus();
        }
        input.value = btn_val;
        return;
      }
    }
  }

  getInputValues() {
    let inputVal = [];
    for (let i of this.inputControls.toArray()) {
      if (i.nativeElement.value)
        inputVal.push(i.nativeElement.value);
    }
    return inputVal.join('');//converts an array into string.
  }
  ngAfterViewInit() {

    this.inputControls.toArray()[0].nativeElement.focus();
  }

  ngOnDestroy() {

  }

  deleteInputValues() {
    for (let i = this.inputControls.toArray().length - 1; i >= 0; i--) {
      let input = this.inputControls.toArray()[i].nativeElement;
      if (input.value) {
        if (i > 0) {
          const prevInput = this.inputControls.toArray()[i - 1].nativeElement;
          prevInput.focus();
        }
        input.value = "";
        return;
      }
    }
  }

  acceptInputValues() {
    const inputsLength = this.getInputValues().length;
    if (inputsLength < 7) {
      this.snackBar.open('INVALID SALES ID.');
    }
  }

}
