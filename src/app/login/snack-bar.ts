import {
    MatSnackBar, MatSnackBarConfig,
    MatSnackBarHorizontalPosition,
    MatSnackBarVerticalPosition
} from "@angular/material";
import { Injectable } from "@angular/core";

@Injectable()
export class SnackBar {
    message: string = 'Invalid Sales Id';
    actionButtonLabel: string = 'Dismiss';
    action: boolean = true;
    setAutoHide: boolean = true;
    autoHide: number = 5000;
    horizontalPosition: MatSnackBarHorizontalPosition = 'left';
    verticalPosition: MatSnackBarVerticalPosition = 'bottom';

    addExtraClass: boolean = false;

    constructor(private _snackBar: MatSnackBar) { }
    open(message) {
        let config = new MatSnackBarConfig();
        config.verticalPosition = this.verticalPosition;
        config.horizontalPosition = this.horizontalPosition;
        config.duration = this.setAutoHide ? this.autoHide : 0;
        //config.panelClass = ['test'];
        this._snackBar.open(message ? message : this.message, this.action ? this.actionButtonLabel : undefined, config);
    }


}