import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material-module/material.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RoutesModule } from './routes-module/routes.module';
import { SnackBar } from 'src/app/login/snack-bar';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule, FormsModule,ReactiveFormsModule, NoopAnimationsModule, MaterialModule, RoutesModule
  ],
  providers: [SnackBar],
  bootstrap: [AppComponent]
})
export class AppModule { }
